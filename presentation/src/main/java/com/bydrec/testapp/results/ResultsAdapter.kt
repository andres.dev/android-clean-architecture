package com.bydrec.testapp.results

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bydrec.testapp.R
import com.bydrec.testapp.domain.model.ResultsMatchItem
import kotlinx.android.synthetic.main.header_item.view.*
import kotlinx.android.synthetic.main.results_item.view.*

class ResultsAdapter(private val items: List<ResultsMatchItem>) : RecyclerView.Adapter<ResultsAdapter.ViewHolder>() {

    open class ViewHolder(view: View) : RecyclerView.ViewHolder(view){
        open fun bind(item: ResultsMatchItem){}
    }

    class ViewHolderHeader(view: View) : ViewHolder(view) {

        override fun bind(item: ResultsMatchItem) {
            with(itemView){
                txtHeader.text = item.headerDate
            }
        }

    }

    class ViewHolderItem(view: View) : ViewHolder(view) {

        override fun bind(item: ResultsMatchItem) {
            with(itemView){
                txtAwayTeam.text = item.awayTeam
                txtCompetitionName.text = item.competitionName
                txtDateAndTime.text = item.dateAndTime
                txtHomeScore.text = item.homeScore.toString()
                txtAwayScore.text = item.awayScore.toString()
                txtHomeTeam.text = item.homeTeam
                txtVenueName.text = item.venueName

                if(item.homeScore?:0 > item.awayScore?:0){
                    txtHomeScore.setTextColor(ContextCompat.getColor(context, R.color.colorAccent))
                    txtAwayScore.setTextColor(ContextCompat.getColor(context, R.color.colorAccentDark))
                } else if (item.homeScore?:0 < item.awayScore?:0){
                    txtHomeScore.setTextColor(ContextCompat.getColor(context, R.color.colorAccentDark))
                    txtAwayScore.setTextColor(ContextCompat.getColor(context, R.color.colorAccent))
                } else {
                    txtHomeScore.setTextColor(ContextCompat.getColor(context, R.color.colorAccentDark))
                    txtAwayScore.setTextColor(ContextCompat.getColor(context, R.color.colorAccentDark))
                }

            }
        }

    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return if(viewType == HEADER) {
            ViewHolderHeader(LayoutInflater.from(parent.context).inflate(R.layout.header_item, parent, false))
        } else{
            ViewHolderItem(LayoutInflater.from(parent.context).inflate(R.layout.results_item, parent, false))
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        return if(items[position].isHeader) HEADER else ITEM
    }

    companion object{
        const val HEADER = 0
        const val ITEM = 1
    }

}