package com.bydrec.testapp.results

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.bydrec.testapp.R
import com.bydrec.testapp.domain.model.ResultsMatchItem
import kotlinx.android.synthetic.main.results_fragment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class ResultsFragment : Fragment() {

    companion object {
        fun newInstance() = ResultsFragment()
    }

    val resultsViewModel : ResultsViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.results_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        rvwItems.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)

        resultsViewModel.getCompetitions().observe(this, Observer<List<String>>{ competitions ->
            val adapter = ArrayAdapter(activity, R.layout.filter_item, R.id.txtTitle, competitions)
            spnFilter.adapter = adapter
        })

        resultsViewModel.getItems().observe(this, Observer<List<ResultsMatchItem>>{ items ->
            rvwItems.adapter = ResultsAdapter(items)
        })

        spnFilter.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                resultsViewModel.loadItems(spnFilter.selectedItem.toString())
            }

        }

    }

}
