package com.bydrec.testapp.results

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel;
import com.bydrec.testapp.domain.model.ResultsMatchItem
import com.bydrec.testapp.domain.usecases.ResultsUseCase
import kotlinx.coroutines.*

class ResultsViewModel(private val resultsUseCase: ResultsUseCase) : ViewModel() {
    private val job = Job()

    private val uiScope = CoroutineScope(Dispatchers.Main + job)

    //items

    private val items: MutableLiveData<List<ResultsMatchItem>> by lazy {
        MutableLiveData<List<ResultsMatchItem>>().also {
            loadItems(ResultsUseCase.ALL_COMPETITIONS)
        }
    }

    fun getItems(): LiveData<List<ResultsMatchItem>> {
        return items
    }

    fun loadItems(competition: String) {
        uiScope.launch {
            items.value = resultsUseCase.getItems(competition)
        }
    }

    //competitions

    private val competitions: MutableLiveData<List<String>> by lazy {
        MutableLiveData<List<String>>().also {
            loadCompetitions()
        }
    }

    fun getCompetitions(): LiveData<List<String>> {
        return competitions
    }

    private fun loadCompetitions() {
        uiScope.launch {
            competitions.value = resultsUseCase.getCompetitions()
        }
    }


}