package com.bydrec.testapp

import android.app.Application
import com.bydrec.testapp.fixtures.fixturesModule
import com.bydrec.testapp.results.resultsModule
import com.jakewharton.threetenabp.AndroidThreeTen
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.dsl.koinApplication

class TestApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)
        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@TestApplication)
            loadKoinModules(fixturesModule, resultsModule)
        }
    }

}