package com.bydrec.testapp.main

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.bydrec.testapp.R
import com.bydrec.testapp.fixtures.FixturesFragment
import com.bydrec.testapp.results.ResultsFragment
import java.lang.NullPointerException


class MainPagerAdapter(private val mContext: Context, fm: FragmentManager) : FragmentPagerAdapter(fm) {

    // This determines the fragment for each tab
    override fun getItem(position: Int): Fragment {
        return when(position){
            0 -> {FixturesFragment()}
            1 -> {ResultsFragment()}
            else -> {throw NullPointerException()}
        }
    }

    // This determines the number of tabs
    override fun getCount(): Int {
        return 2
    }

    // This determines the title for each tab
    override fun getPageTitle(position: Int): CharSequence? {
        // Generate title based on item position
        return when (position) {
            0 -> mContext.getString(R.string.tab_fixtures)
            1 -> mContext.getString(R.string.tab_results)
            else -> null
        }
    }

}