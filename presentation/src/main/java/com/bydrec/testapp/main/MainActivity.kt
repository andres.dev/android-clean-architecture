package com.bydrec.testapp.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bydrec.testapp.R
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val adapter = MainPagerAdapter(this, supportFragmentManager)
        vpgMain.adapter = adapter
        tblMain.setupWithViewPager(vpgMain)

    }
}
