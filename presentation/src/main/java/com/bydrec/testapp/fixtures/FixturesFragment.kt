package com.bydrec.testapp.fixtures

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bydrec.testapp.R
import com.bydrec.testapp.domain.model.FixturesMatchItem
import kotlinx.android.synthetic.main.fixtures_fragment.rvwItems
import kotlinx.android.synthetic.main.fixtures_fragment.spnFilter
import org.koin.androidx.viewmodel.ext.android.viewModel

class FixturesFragment : Fragment() {

    companion object {
        fun newInstance() = FixturesFragment()
    }

    val fixturesViewModel : FixturesViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fixtures_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        rvwItems.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)


        fixturesViewModel.getCompetitions().observe(this, Observer<List<String>>{ competitions ->
            val adapter = ArrayAdapter(activity, R.layout.filter_item, R.id.txtTitle, competitions)
            spnFilter.adapter = adapter
        })

        fixturesViewModel.getItems().observe(this, Observer<List<FixturesMatchItem>>{ items ->
            rvwItems.adapter = FixturesAdapter(items)

        })

        spnFilter.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                fixturesViewModel.loadItems(spnFilter.selectedItem.toString())
            }

        }

    }

}
