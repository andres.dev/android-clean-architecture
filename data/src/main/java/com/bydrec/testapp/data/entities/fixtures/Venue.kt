package com.bydrec.testapp.data.entities.fixtures

data class Venue(
	val name: String? = null,
	val id: Int? = null
)
