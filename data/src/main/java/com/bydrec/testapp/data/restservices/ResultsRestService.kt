package com.bydrec.testapp.data.restservices

import com.bydrec.testapp.data.entities.results.ResultsResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET

interface ResultsRestService {

    @GET(value = "/cdn-og-test-api/test-task/results.json")
    fun results(): Deferred<List<ResultsResponse>>

}