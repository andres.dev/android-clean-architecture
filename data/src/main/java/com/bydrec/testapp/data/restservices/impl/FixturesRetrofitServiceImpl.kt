package com.bydrec.testapp.data.restservices.impl

import com.bydrec.testapp.data.entities.fixtures.FixturesResponse
import com.bydrec.testapp.data.restservices.FixturesRestService
import com.bydrec.testapp.data.restservices.RetrofitService
import kotlinx.coroutines.Deferred

class FixturesRetrofitServiceImpl : FixturesRestService, RetrofitService<FixturesRestService>(FixturesRestService::class.java) {

    override fun fixtures(): Deferred<List<FixturesResponse>> {
        return service.fixtures()
    }

}