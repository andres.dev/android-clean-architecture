package com.bydrec.testapp.data.entities.results

data class CompetitionStage(
	val competition: Competition? = null
)
