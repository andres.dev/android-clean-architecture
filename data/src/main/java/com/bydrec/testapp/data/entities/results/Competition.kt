package com.bydrec.testapp.data.entities.results

data class Competition(
	val name: String? = null,
	val id: Int? = null
)
