package com.bydrec.testapp.data.entities.fixtures

data class CompetitionStage(
	val competition: Competition? = null
)
