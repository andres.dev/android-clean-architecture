package com.bydrec.testapp.data.entities.results

data class HomeTeam(
	val name: String? = null,
	val alias: String? = null,
	val id: Int? = null,
	val shortName: String? = null,
	val abbr: String? = null
)
