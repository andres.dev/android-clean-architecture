package com.bydrec.testapp.data.restservices

import com.bydrec.testapp.data.entities.fixtures.FixturesResponse
import kotlinx.coroutines.Deferred
import retrofit2.http.GET

interface FixturesRestService {

    @GET(value = "/cdn-og-test-api/test-task/fixtures.json")
    fun fixtures(): Deferred<List<FixturesResponse>>

}