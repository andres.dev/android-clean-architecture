package com.bydrec.testapp.data.entities.fixtures

import com.bydrec.testapp.domain.model.FixturesMatchItem
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId
import org.threeten.bp.ZoneOffset
import org.threeten.bp.format.DateTimeFormatter

data class FixturesResponse(
	val date: String? = null,
	val venue: Venue? = null,
	val awayTeam: AwayTeam? = null,
	val homeTeam: HomeTeam? = null,
	val id: Int? = null,
	val state: String? = null,
	val type: String? = null,
	val competitionStage: CompetitionStage? = null
) {

	companion object{

		fun transform(response: FixturesResponse): FixturesMatchItem{


			val utcDateTime = LocalDateTime.parse(response.date, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")).atZone(
				ZoneId.of("UTC"))

			val localDateTime = utcDateTime.withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime()

			return FixturesMatchItem(
				response.competitionStage?.competition?.name.orEmpty(),
				response.venue?.name.orEmpty(),
				localDateTime.format(DateTimeFormatter.ofPattern("MMM dd, yyyy 'at' HH:mm")),
				response.homeTeam?.name.orEmpty(),
				response.awayTeam?.name.orEmpty(),
				localDateTime.dayOfMonth,
				localDateTime.dayOfWeek,
				response.state == "postponed",
				false,
				localDateTime.format(DateTimeFormatter.ofPattern("LLLL yyyy")),
				localDateTime.toInstant(ZoneOffset.UTC).toEpochMilli()
			)
		}

	}

}
