package com.bydrec.testapp.data.entities.results

import com.bydrec.testapp.domain.model.ResultsMatchItem
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneId
import org.threeten.bp.ZoneOffset
import org.threeten.bp.format.DateTimeFormatter

data class ResultsResponse(
	val date: String? = null,
	val venue: Venue? = null,
	val score: Score? = null,
	val awayTeam: AwayTeam? = null,
	val homeTeam: HomeTeam? = null,
	val id: Int? = null,
	val state: String? = null,
	val type: String? = null,
	val competitionStage: CompetitionStage? = null
) {

	companion object {

		fun transform(response: ResultsResponse): ResultsMatchItem {

			val utcDateTime = LocalDateTime.parse(response.date, DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")).atZone(
				ZoneId.of("UTC"))

			val localDateTime = utcDateTime.withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime()

			return ResultsMatchItem(
				response.competitionStage?.competition?.name,
				response.venue?.name,
				localDateTime.format(DateTimeFormatter.ofPattern("LLL dd, yyyy 'at' HH:mm")),
				response.homeTeam?.name,
				response.awayTeam?.name,
				response.score?.home ?: 0,
				response.score?.away ?: 0,
				false,
				localDateTime.format(DateTimeFormatter.ofPattern("LLLL yyyy")),
				localDateTime.toInstant(ZoneOffset.UTC).toEpochMilli()
			)
		}

	}
}