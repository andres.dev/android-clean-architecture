package com.bydrec.testapp.data.repositories

import com.bydrec.testapp.data.entities.fixtures.FixturesResponse
import com.bydrec.testapp.data.restservices.FixturesRestService
import com.bydrec.testapp.domain.model.FixturesMatchItem
import com.bydrec.testapp.domain.repositories.FixturesRepository

class FixturesRepositoryImpl(val fixturesRestService: FixturesRestService) : FixturesRepository{

    suspend override fun getItems(): List<FixturesMatchItem> {
        val itemList = mutableListOf<FixturesMatchItem>()
        val fixtureResponse = fixturesRestService.fixtures().await()
        fixtureResponse.forEach {
            itemList.add(FixturesResponse.transform(it))
        }
        return itemList
    }

}