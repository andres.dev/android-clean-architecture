package com.bydrec.testapp.data.entities.fixtures

data class Competition(
	val name: String? = null,
	val id: Int? = null
)
