package com.bydrec.testapp.domain.repositories

import com.bydrec.testapp.domain.model.FixturesMatchItem

interface FixturesRepository {
    suspend fun getItems(): List<FixturesMatchItem>
}