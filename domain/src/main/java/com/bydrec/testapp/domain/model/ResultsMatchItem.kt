package com.bydrec.testapp.domain.model

data class ResultsMatchItem(
    val competitionName: String? = null,
    val venueName: String? = null,
    val dateAndTime: String? = null,
    val homeTeam: String? = null,
    val awayTeam: String? = null,
    val homeScore: Int? = null,
    val awayScore: Int? = null,
    var isHeader : Boolean = false,
    var headerDate: String? = null,
    var time: Long? = null
)
