package com.bydrec.testapp.domain.repositories

import com.bydrec.testapp.domain.model.ResultsMatchItem

interface ResultsRepository {
    suspend fun getItems(): List<ResultsMatchItem>
}