package com.bydrec.testapp.domain.usecases

import com.bydrec.testapp.domain.model.FixturesMatchItem
import com.bydrec.testapp.domain.repositories.FixturesRepository

class FixturesUseCase(private val fixturesRepository: FixturesRepository) {

    suspend fun getItems(competition: String): List<FixturesMatchItem>{
        val headerDateList = mutableListOf<String>()
        val flattenedList = mutableListOf<FixturesMatchItem>()
        val originalList = if(competition == ALL_COMPETITIONS) {
            fixturesRepository.getItems().sortedBy { it.time }
        } else {
            fixturesRepository.getItems().filter { competition == it.competitionName }.sortedBy { it.time }
        }
        originalList.forEach {
            if(it.headerDate !in headerDateList) {
                it.headerDate?.let { it1 -> headerDateList.add(it1) }
            }
        }
        headerDateList.forEach {headerDate ->
            //adding header (month and year)
            flattenedList.add(
                FixturesMatchItem(isHeader = true, headerDate =  headerDate)
            )
            //adding items
            flattenedList.addAll(originalList.filter { it.headerDate == headerDate })
        }
        return flattenedList
    }

    suspend fun getCompetitions(): List<String>{
        val originalList = fixturesRepository.getItems().sortedBy { it.competitionName }

        val competitionList = mutableListOf(ALL_COMPETITIONS)
        originalList.forEach {
            if(it.competitionName !in competitionList) {
                it.competitionName?.let { it1 -> competitionList.add(it1) }
            }
        }
        return competitionList
    }

    companion object{
        const val ALL_COMPETITIONS = "All competitions"
    }

}